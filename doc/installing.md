# Installing node-flesh

## Prerequisites

- [prerequisites](./prerequisites)

## Development

```shell
git clone https://gitlab.com/elioway/elioway.gitlab.io.git elioway
cd elioway
git clone https://gitlab.com/elioway/elioflesh.git
cd elioflesh
git clone https://gitlab.com/elioflesh/node-flesh.git
```
