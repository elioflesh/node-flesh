const should = require("chai").should()

const { tattoo, pick } = require("../flesh/tattoo.js")

describe("tattoo", () => {
  const obj = {
    color: "blue",
    name: { plural: "octopi", single: "octopus" },
  }
  it("basics", () => {
    tattoo("The {{name.single}} is {{color}}", obj).should.eql(
      "The octopus is blue"
    )
    tattoo("The {{name.plural}} is {{color}}", obj).should.eql(
      "The octopi is blue"
    )
  })
  it("faulty", () => {
    tattoo("The {{name.nada}} is {{color}}", obj).should.eql("The  is blue")
  })
})

describe("pick", () => {
  const obj = { a: { b: { c: { d: { e: 5 }, f: 7, g: () => 8 } } }, z: 26 }
  it("pathy", () => {
    pick("a.b.c.d.e", obj).should.eql(5)
    pick("a.b.c.f", obj).should.eql(7)
  })
  it("short", () => {
    pick("z", obj).should.eql(26)
  })
  it("returns obj", () => {
    pick("a.b.c.d", obj).should.eql({ e: 5 })
    pick("a.b.c.g", obj).should.eql(obj.a.b.c.g)
    pick("a.b.c.g", obj)().should.eql(8)
  })
  it("short missing path", () => {
    pick("y", obj).should.eql("")
  })
  it("no path", () => {
    pick("a.b.c.e", obj).should.eql("")
  })
  it("long faulty path", () => {
    pick("a.b.c.d.e.f", obj).should.eql("")
  })
  it("deeply faulty path", () => {
    pick("x.v.u.t.s.r", obj).should.eql("")
  })
  it("obj parameter missing or empty", () => {
    pick("a.b").should.eql("")
    pick("a.b", {}).should.eql("")
  })
  it("varied empty values", () => {
    pick("a", { a: "" }).should.eql("", "empty string")
    pick("a", { a: {} }).should.eql({}, "empty object")
    pick("a", { a: " " }).should.eql(" ", "space")
  })
  it("null", () => {
    pick("a", { a: null }).should.eql("")
  })
  it("undefined", () => {
    pick("a", { a: undefined }).should.eql("")
  })
  it("true", () => {
    pick("a", { a: true }).should.be.true
  })
  it("false", () => {
    pick("a", { a: false }).should.eql("")
  })
  it("path blocked by non object", () => {
    pick("a.b", { a: 1 }).should.eql("")
  })
})
