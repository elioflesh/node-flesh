const { getStaticAsset } = require("../helpers")

const CONTENTTYPES = {
  "png": "image/png",
  "jpg": "image/jpeg",
  "ico": "image/x-icon",
  "css": "text/css",
  "plain": "text/plain",
  "map": "text/plain",
}

const getStatus
/**
 * Return a static file from the public folder.
 */
const staticT = (handle, cb) => {
  const { way, method } = handle
  if (method === "get") {
    getStaticAsset(way, (err, fileContents) => {
      if (!err) {
        let fileType = way.split(".")[1]
        cb(false,fileContents,CONTENTTYPES[fileType])
      } else {
        cb(405)
      }
    })
  } else {
    cb(405)
  }
}

module.exports = staticT
