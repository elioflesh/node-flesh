const PERMITLEVELS = require("../permits")
const { permitT } = require("../../../../eliobones/node-bones/client")

const isGOD = (engagedData, permitAudience) => {
  /** @TODO APP.god? */
  return (
    engagedData.identifier === permitAudience ||
    engagedData.god === permitAudience
  )
}

const isLISTED = (engagedData, permitAudience) => {
  return (
    engagedData.itemList &&
    engagedData.itemList.map(i => i.indentifier).includes(permitAudience)
  )
}

const permitT = (permitData, engagedData, method, way, LISTTHINGTYPE, cb) => {
  // Route auth/delete token (aka `signoutT`) should always be level GOD.
  let permittedLevel = PERMITLEVELS.GOD
  if (way !== "auth") {
    // Permits active? Else leave at default "GOD"
    if (engagedData.hasOwnProperty("permits")) {
      // Get the permittedLevel
      permittedLevel = engagedData.permits[way][method]
      if (way === "list" && typeof permittedLevel == "object") {
        // Permission may be specific to ThingType.
        if (permittedLevel.hasOwnProperty(LISTTHINGTYPE)) {
          permittedLevel = permittedLevel[LISTTHINGTYPE]
        } else {
          // No THINGTYPE or not covered by PERMITS, resort to default.
          permittedLevel = PERMITLEVELS.GOD
        }
      }
    }
  }

  if (!err && permitData) {
    let { permitAudience, validUntil } = permitData.engage.Permit
    // Check token is still valid.
    if (validUntil > Date.now()) {
      // Check permission level + relationship of token owner to Engaged Thing:
      // - token owner is GOD to Engaged Thing OR
      // - token owner is LISTED in Engaged Thing OR
      // - token proves authenticated owner.
      if (
        isGOD(engagedData, permitAudience) ||
        (isLISTED(engagedData, permitAudience) &&
          PERMITLEVELS.LISTED === permittedLevel) ||
        PERMITLEVELS.AUTH === permittedLevel
      ) {
        cb(true)
      } else {
        cb(false, {
          Error: `Permission denied. ${permittedLevel} level required.`,
        })
      }
    } else {
      cb(false, { Error: "Permit not valid." })
    }
  } else {
    cb(false, { Error: "Permit not found." })
  }

  if (permittedLevel === PERMITLEVELS.ANON) {
    // then Token is not required to perform this action...
    // AKA... allow anonymous client access.
    cb(true)
  }
}

module.exports = permitT
