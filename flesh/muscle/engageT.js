const { engageT } = require("../../../../eliobones/node-bones/client")

const engageT = (handle, cb) => {
  let { identifier, THINGTYPE } = handle
  if (identifier) {
    db.read(THINGTYPE, identifier, (err, engagedData) => {
      if (!err && engagedData) {
        cb(true, {}, engagedData)
      } else {
        cb(false, { Error: `${identifier} Thing not found.` })
      }
    })
  } else {
    cb(false, { Error: "Missing `identifier`." })
  }
}

module.exports = engageT
