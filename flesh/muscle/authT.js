const engageT = require("./engageT")
const permitT = require("./permitT")

const authT = (handle, cb) => {
  engageT(handle, (exists, err, engagedData) => {
    // The thing being engaged exists
    if (exists) {
      // `permit` is the client token which was submitted.
      let { permit } = header
      // Does the client have a token?
      if (permit) {
        // Examine the token.
        db.read("Permit", permit, (err, tokenData) => {
          if (err) {
            resolve(tokenData)
          } else {
            cb(false, { Error: "No `permit` found." })
          }
        })
      } else {
        cb(false, { Error: "No `permit` supplied." })
      }
    } else {
      resolve()
    }
  })
}

module.exports = authT
