//* A router in all but name. */
// const takeonT = require("./skin/takeonT")
// const deleteT = require("./skin/deleteT")
// const enlistT = require("./skin/enlistT")
// const listT = require("./skin/listT")
// const loginT = require("./skin/loginT")
// const logoutT = require("./skin/logoutT")
// const readT = require("./skin/readT")
// const schemaT = require("./skin/schemaT")
// const takeupT = require("./skin/takeupT")
// const unlistT = require("./skin/unlistT")
// const updateT = require("./skin/updateT")
const { getTemplate } = require("./helpers")

const authRoutes = {
  // post: loginT,
  // delete: logoutT,
}

const engageRoutes = {
  // post: takeupT,
  // get: readT,
  // patch: updateT,
  // delete: deleteT,
}

const listRoutes = {
  // post: takeonT,
  // get: listT,
  // put: enlistT,
  // delete: unlistT,
}

module.exports = (handle, cb) => {
  const { way, method } = handle
  switch (way) {
    case "auth":
      authRoutes[method](handle, cb)
      break
    case "engage":
      engageRoutes[method](handle, cb)
      break
    case "list":
      listRoutes[method](handle, cb)
      break
    case "ping":
      getTemplate("ping", cb)
      break
    case "schema":
      // schemaT(handle, cb)
      break
    case "public":
    case "favicon.ico":
    default:
      getStaticAsset(404, "URL Endpoint not valid.")
  }
}
