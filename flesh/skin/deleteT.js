const { deleteT } = require("../../../../eliobones/node-bones/client")
const helpers = require("../helpers")
const authT = require("../muscle/authT")

module.exports = (handle, cb) => {
  authT(handle, (permitted, err, _) => {
    if (permitted) {
      let { identifier, THINGTYPE } = handle
      deleteT(THINGTYPE, identifier, err => {
        if (!err) {
          cb(200, { Message: `${identifier} Thing deleted.` })
        } else {
          cb(500, {
            Error: `Could not delete ${identifier} Thing.`,
            Reason: err,
          })
        }
      })
    } else {
      cb(400, err)
    }
  })
}
