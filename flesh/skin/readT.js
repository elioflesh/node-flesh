const { readT } = require("../../../../eliobones/node-bones/client")
const helpers = require("../helpers")
const authT = require("../muscle/authT")

const readT = (handle, cb) => {
  authT(handle, (permitted, err, engagedData) => {
    if (permitted && engagedData) {
      let { header, identifier } = handle
      if (permitted && engagedData) {
        delete engagedData.password
        cb(200, engagedData)
      } else {
        cb(404, err)
      }
    } else {
      cb(404, err)
    }
  })
}

module.exports = readT
