const { loginT } = require("../../../../eliobones/node-bones/client")
const {
  hash,
  hasRequiredFields,
  makeIdentifier,
  makePermitIdentifier,
} = require("../helpers")
const engageT = require("../muscle/engageT")

const loginT = (handle, cb) => {
  let { packet } = handle
  if (hasRequiredFields(packet, ["name", "password"])) {
    /** @TODO Terrible! Do something better for Id. */
    let identifier = makeIdentifier(packet)
    engageT({ ...handle, identifier }, (exists, err, engagedData) => {
      if (exists) {
        let password = packet.password.trim()
        let hashedPassword = hash(password)
        if (hashedPassword == engagedData.password) {
          let permit = makePermitIdentifier()
          let validUntil = Date.now() + 1000 * 60 * 60
          let tokenData = {
            identifier: permit,
            engage: {
              Permit: {
                validUntil,
                permitAudience: identifier,
              },
            },
          }
          db.create("Permit", permit, tokenData, err => {
            if (!err) {
              cb(200, tokenData)
            } else {
              cb(400, {
                Error: `Error creating permit for ${identifier} Thing.`,
                Reason: err,
              })
            }
          })
        } else {
          cb(400, {
            Error: `${identifier} Thing's password was incorrect.`,
          })
        }
      } else {
        cb(400, {
          Error: `Could not find ${identifier} Thing.`,
          Reason: err,
        })
      }
    })
  } else {
    cb(400, {
      Error: `Thing missing required fields for login.`,
    })
  }
}

module.exports = login
