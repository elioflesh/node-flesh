const { enlistT } = require("../../../../eliobones/node-bones/client")
const helpers = require("../helpers")
const authT = require("../muscle/authT")

/**
 * @TODO Resolve the required delay in calling this endpoint.
 * @ISSUE We're not always adding to the current list for each enlistT (e.g. if called 10 times quickly and asynchrously).
 */
const enlistT = (handle, cb) => {
  authT(handle, (permitted, err, engagedData) => {
    if (permitted && engagedData) {
      let { identifier, listIdentifier, packet, LISTTHINGTYPE, THINGTYPE } =
        handle
      let engagedList = new Set(engagedData.itemList || [])
      let listKey = [...engagedList.values()].find(
        item => item.identifier === listIdentifier
      )
      if (!engagedList.has(listKey)) {
        engagedList.add({
          identifier: listIdentifier,
          mainEntityOfPage: LISTTHINGTYPE,
        })
        engagedData.itemList = [...engagedList]
        db.update(THINGTYPE, identifier, engagedData, err => {
          if (!err) {
            delete engagedData.password
            cb(200, engagedData)
          } else {
            cb(500, {
              Error: `Could not enlist ${identifier} Thing.`,
              Reason: err,
            })
          }
        })
      } else {
        cb(200, { Message: `${identifier} Thing already listed.` })
      }
    } else {
      cb(400, err)
    }
  })
}

module.exports = enlistT
