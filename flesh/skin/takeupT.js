const { takeupT } = require("../../../../eliobones/node-bones/client")
const { hash, hasRequiredFields, makeIdentifier, url } = require("../helpers")

const takeupT = (handle, cb) => {
  let { packet, THINGTYPE } = handle
  if (hasRequiredFields(packet, ["name"])) {
    let identifier = makeIdentifier(packet)
    db.exists(THINGTYPE, identifier, exists => {
      if (!exists) {
        let hashedPassword = ""
        if (packet.password) {
          hashedPassword = hash(packet.password.trim())
        }
        let engagedData = {
          ...packet,
          identifier,
          mainEntityOfPage: THINGTYPE,
          url: url(THINGTYPE, identifier),
          password: hashedPassword,
        }
        db.create(THINGTYPE, identifier, engagedData, err => {
          if (!err) {
            delete engagedData.password
            cb(200, engagedData)
          } else {
            cb(500, {
              Error: `Could not create ${identifier} Thing.`,
              Reason: err,
            })
          }
        })
      } else {
        cb(400, { Error: `${identifier} Thing already exists. Please login.` })
      }
    })
  } else {
    cb(400, { Error: `Thing missing required fields.` })
  }
}

module.exports = takeupT
