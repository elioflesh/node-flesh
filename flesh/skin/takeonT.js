const { takeonT } = require("../../../../eliobones/node-bones/client")
const { hasRequiredFields, makeIdentifier } = require("../helpers")
const authT = require("../muscle/authT")
const enlistT = require("./enlistT")

const takeonT = (handle, cb) => {
  authT(handle, (permitted, err, _) => {
    let { packet, LISTTHINGTYPE } = handle
    if (hasRequiredFields(packet, ["name"])) {
      let listIdentifier = packet.identifier
      if (!listIdentifier) {
        listIdentifier = makeIdentifier(packet)
        packet.identifier = listIdentifier
      }
      db.exists(LISTTHINGTYPE, listIdentifier, (exists, err) => {
        if (!exists) {
          db.create(LISTTHINGTYPE, listIdentifier, packet, err => {
            if (!err) {
              enlistT({ ...handle, listIdentifier }, cb)
            } else {
              cb(500, {
                Error: `Could not create ${listIdentifier} Thing.`,
                Reason: err,
              })
            }
          })
        } else {
          enlistT({ ...handle, listIdentifier }, cb)
        }
      })
    } else {
      cb(400, {
        Error: `${listIdentifier} Thing is missing the required fields.`,
      })
    }
  })
}

module.exports = takeonT
