const { deleteT } = require("../../../../eliobones/node-bones/client")
const helpers = require("../helpers")
const authT = require("../muscle/authT")

const logoutT = (handle, cb) => {
  authT(handle, (permitted, err, _) => {
    if (permitted) {
      let { header, identifier } = handle
      db.delete("Permit", header.permit, err => {
        if (!err) {
          cb(200, { Message: `${identifier} Thing logout.` })
        } else {
          cb(500, {
            Error: `Could not logout ${identifier} Thing.`,
            Reason: err,
          })
        }
      })
    } else {
      cb(400, err)
    }
  })
}

module.exports = logoutT
