const { listT } = require("../../../../eliobones/node-bones/client")
const helpers = require("../helpers")
const authT = require("../muscle/authT")

const listT = (handle, cb) => {
  authT(handle, (permitted, err, engagedData) => {
    if (permitted && engagedData) {
      let { identifier, packet, LISTTHINGTYPE, THINGTYPE } = handle
      if (engagedData.itemList) {
        let engagedList = [...engagedData.itemList]
        if (LISTTHINGTYPE) {
          engagedList = engagedList.filter(
            item => item.mainEntityOfPage === LISTTHINGTYPE
          )
        }
        db.list(engagedList, (err, listData) => {
          if (!err) {
            cb(200, listData)
          } else {
            cb(500, {
              Error: `Could not get ${identifier} Thing's list.`,
              Reason: err,
            })
          }
        })
      } else {
        cb(200, { Message: `${identifier} Thing list is empty.` })
      }
    } else {
      cb(400, err)
    }
  })
}

module.exports = listT
