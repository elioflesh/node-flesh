const { unlistT } = require("../../../../eliobones/node-bones/client")
const helpers = require("../helpers")
const authT = require("../muscle/authT")

const unlistT = (handle, cb) => {
  authT(handle, (permitted, err, engagedData) => {
    if (permitted && engagedData) {
      let { identifier, listIdentifier, packet, THINGTYPE } = handle
      let engagedList = new Set(engagedData.itemList || [])
      let listKey = [...engagedList.values()].find(
        item => item.identifier === listIdentifier
      )
      if (listKey && engagedList.delete(listKey)) {
        engagedData.itemList = [...engagedList]
        db.update(THINGTYPE, identifier, engagedData, err => {
          if (!err) {
            delete engagedData.password
            cb(200, engagedData)
          } else {
            cb(500, {
              Error: `Could not unlistT ${identifier} Thing.`,
              Reason: err,
            })
          }
        })
      } else {
        cb(200, { Message: `${identifier} Thing wasn't listed.` })
      }
    } else {
      cb(400, err)
    }
  })
}

module.exports = unlistT
