const { updateT } = require("../../../../eliobones/node-bones/client")
const { hash } = require("../helpers")
const authT = require("../muscle/authT")

const updateT = (handle, cb) => {
  authT(handle, (permitted, err, engagedData) => {
    if (permitted && engagedData) {
      let { identifier, packet, THINGTYPE } = handle
      if (packet.password) {
        packet.password = hash(packet.password)
      }
      let updatePacket = {
        ...engagedData,
        ...packet,
      }
      db.update(THINGTYPE, identifier, updatePacket, err => {
        if (!err) {
          delete updatePacket.password
          cb(200, updatePacket)
        } else {
          cb(500, {
            Error: `${identifier} Thing could not be updated.`,
            Reason: err,
          })
        }
      })
    } else {
      cb(404, err)
    }
  })
}

module.exports = updateT
