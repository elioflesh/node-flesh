//* A misc library of helper functions. */
const config = require("./config")
const path = require("path")
const fs = require("fs")

var helpers = {}

helpers.getTemplate = (templateName, cb) => {
  templateName = templateName ? templateName : "index"
  let templateDir = path.join(__dirname, "/../templates/")
  fs.readFile(`${templateDir}/${templateName}.html`, "utf8", (err, str) => {
    if (err) {
      cb(err, "")
    } else {
      cb(false, str)
    }
  })
}

helpers.getStaticAsset = (assetName, cb) => {
  // let publicDir = path.join(__dirname, "/../public/")
  fs.readFile(assetName, (err, assetContents) => {
    if (err) {
      cb("No file could be found", err)
    } else {
      cb(false, assetContents)
    }
  })
}

module.exports = helpers
