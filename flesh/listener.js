//* Listens for requests to the API; parses the request object; marshalls the call to the router. */

const url = require("url")
const { StringDecoder } = require("string_decoder")
const skinT = require("./skinT")
// const helpers = require("./helpers")

const stringDecoder = new StringDecoder("utf-8")

const listener = (req, res) => {
  let header = req.headers
  var method = req.method.toLowerCase()
  let parsedUrl = url.parse(req.url, true)
  // Extract PATHs.
  let path = parsedUrl.pathname.replace(/^\/+|\/+$/g, "")
  let way = path.split("/")[0]
  let THINGTYPE = path.split("/")[1]
  let identifier = path.split("/")[2]
  let LISTTHINGTYPE = path.split("/")[3]
  let listIdentifier = path.split("/")[4]
  // Get QUERYSTRING.
  let qs = parsedUrl.query
  // Buffer request DATA.
  let packet = ""
  req.on("data", buffer => {
    packet += stringDecoder.write(buffer)
  })
  req.on("end", () => {
    packet += stringDecoder.end()
    let handle = {
      header,
      method,
      packet,
      qs,
      identifier,
      listIdentifier,
      LISTTHINGTYPE,
      THINGTYPE,
      way,
    }
    skinT(handle, (status, payload, contentType) => {
      status = status || 200
      payload = typeof payload === "string" ? payload : ""
      res.setHeader("Content-Type", contentType || "text/html")
      res.writeHead(status)
      res.end(payload)
      console.info(`${method.toUpperCase()} /${path} (${status})`)
    })
  })
}

module.exports = listener
