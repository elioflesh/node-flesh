/** @file Tattoo: A simple templating engine. */

// RegExp finding all double braces and their contents.
const RE_DOUBLEBRACES = /{{\s*[\w\.]+\s*}}/g

/** Resolve the value of an object using string version of "dot" notation.
 * @usage
 >> let firstName = pick(
 >>   "name.firstName",
 >>   { name: {firstName: Tim, lastName: Bushell }}
 >> )
 * @param {String} objectPath of expected value.
 * @param {Object} JSON object containing value at path.
 * @returns {*} value at path or "".
 */
const pick = (objectPath, object) => {
  let keyPath = objectPath.split(".")
  return keyPath.reduce((acc, key) => {
    let pathVal = ""
    if (typeof acc === "object") {
      pathVal = acc[key]
    }
    return pathVal || ""
  }, object)
}

/** A simple templating function.
* @usage
>> tattoo(
>>  "My name {{firstName}} {{lastName}}",
>>  { firstName: "Tim", lastName: "Bushell"}
>> )
* @param {String} template containing placeholders.
* @param {Object} data object holding values for placing.
*/
const tattoo = (template, data) => {
  // Find all template placeholders.
  let placeholders = [...template.match(RE_DOUBLEBRACES)]
  // Check for any excessive length suggesting syntax error.
  let simpleLenCheck = Math.max(placeholders.map(s => s.length)) > 100
  if (simpleLenCheck) {
    throw Error("One of the template placeholders is long!")
  }
  // Search and replace all placeholders.
  placeholders.forEach(match => {
    let path = match.slice(2, -2)
    let val = pick(path, data)
    template = template.replace(match, val)
  })
  return template
}

module.exports = {
  pick,
  tattoo,
}
