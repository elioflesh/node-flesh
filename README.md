![](elio-node-flesh-logo.png)

> NodeJs, naked, alone, **the elioWay**

# node-flesh

Frontend website in [NodeJs](https://nodejs.org/) with zero dependencies. It serves as a way to learn **NodeJs** and to play-with the anatomy of an **elioFlesh** website (needed to support applications and solutions), the **elioWay**.

## Installing

- [Installing node-flesh](/doc/installing.md)

## Requirements

- [node-flesh Prerequisites](/doc/prerequisites.md)

## Seeing is Believing

```shell
# Start the API server.
node flesh/index.js
```

- **See** [Example Uses](examples/README.md)

## Quickstart

- [node-flesh Quickstart](/doc/quickstart.md)

# Credits

- [node-flesh Credits](/doc/credits.md)

## License

[MIT](LICENSE)

![](apple-touch-icon.png)
