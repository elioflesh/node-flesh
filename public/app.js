console.log("Hello World")

let app = {}

app.config = {
  sessionToken: false
}

app.client = {}

app.client.request = (headers, path, method, queryStringObject, payload, cb) => {
  var xhr = new XMLHttpRequest()
  xhr.open(method, requestUrl, true)
  xhr.setRequestHeader("Content-Type", "application/json")
  Object.entries(headers).forEach(([key,val]) => {
    xhr.setRequestHeader(key, val)
  })
  if (app.config.sessionToken) {
    xhr.setRequestHeader("token", app.config.sessionToken)
  }
  xhr.onstatereadychange = () => {
    if (xhr.readyState===xhr.XMLHttpRequest.DONE) {
      let stateCode = xhr.status
      let responseReturned = xhr.responseText
      if (cb) {
        try {
          let parsedResponse = JSON.parse(responseReturned)
          cb(statusCode, parsedResponse)
        } catch(cbError) {
          cb(statusCode, false)
        }
      }
    }
  }
  let payloadString = JSON.stringify(payload)
  xnr.send(payloadString)
}

module.exports = app
